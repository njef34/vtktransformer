////////////////////////////////
// This program is the intellectual property of
//     NICHOLAS J EGGLESTON
// Created for the Missouri University of Science and Technology
//     IT Research Support Services
//     and Mark Bookout, Director of RSS
// 
// Project: V4DiR (Visualizing 4 Dimensions in Rolla)
// Filename: mpivtk.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#define EXTENSION ".vtk"
#define VALID_DELIMETERS " ,\n\t"
#define LARGE_NUMBER 5000

//#define NULL '\0'  
#define TAB '\t'
#define NUL '\0'

/*======VTK HEADER========*/
#define LINE1 "# vtk DataFile Version 2.0\n"  //Datafile version
#define LINE2 "%s_%s%i\n"                     //Title-of-data_framexxx
#define LINE3 "ASCII\n"                       //Is data ASCII or Binary (In this case ASCII)
#define LINE4 "DATASET UNSTRUCTURED_GRID\n"   //We have an "Unstructured Grid" Dataset
#define LINE5 "POINTS %li float\n"            //Here's our points, here's how many we have, they're of type float

int isnum(char* string){
  int i;
  for (i = 0; (string[i] >= '0' && string[i] <= '9') || string[i] == '.' || string[i] == '-'; i++);
  return !string[i];
  //This is likely to be the most confusing thing ever, the gist is, if we make it all the way through, we hit null, null is 0, if we don't make it all the way through, we have an alphanum
  //When you inverse 0 (null), its true; when you inverse an alphanum, its false.
  //Bam! 3 lines, no ifs
}  

int main(int argc, char *argv[]) {
  printf("hi\n");//////////////////////////
  /* ===== Start Declare Vars =====*/
  printf("declarations\n");///////////////////////
  /* === Hold XYZ Data === */
  float *data = NULL;
  //float data[100];
  int x = -1;
  int y = -1;
  int z = -1;
  /* === Hold Template Data ===*/
  int template_elements;
  int *attributes;
  char delim = NUL;
  /* === Hold File Data == */
  FILE **files; // This holds an array of pointers to attribute files

  FILE *templateFile; // = fopen(TEMPLATE_FILE, "r");
  FILE *infile; //= fopen(INPUT_FILE, "r");
  FILE *outfile;
  FILE *attributesFile;
  FILE *configFile = fopen(argv[1], "r");
  char title[LARGE_NUMBER]; //= "mineData_1_2_5"; //This is sort of a const but eventually we'll prompt for user input
  char outputFileName[256];
  char attributeFileName[256];
  /* === Hold Attribute Data === */
  int red = 6;
  int green = 7;
  int blue = 8;
  int date = -1;
  int time = -1;
  int cols = 1;
  long rows = 0;
  int numAttributes = 0;
  unsigned long realRows = 0;
  unsigned long divRows;   //This holds how many rows will go in each division for a for-loop later
  /* === Other Random Containers === */
  int numDivisions = 1; //This holds the user specified number of divisions for their data
  int i, j, k, point;
  int div;            //This holds which division we're on currently
  unsigned long readStart;
  char tempString[LARGE_NUMBER];
  char in[LARGE_NUMBER];
  char *tok;
  /* ===== End Declare Vars ===== */
  
  /*===== Load values from front-end ====*/ //We need to fix the newline problem here
  printf("Start load from file\n");

  //Get title of dataset
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';
  strcpy(title, in);
  //title = in;
puts(title);

  //Get location of datafile
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';
//printf("%s", in);

  infile = fopen(in, "r");
printf("%p\n", infile);

  //Get location of template file
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';
  
  templateFile = fopen(in, "r");
printf("%p\n", templateFile);

  /*  //Get number of divisions we'd like the file to have
    fgets(in, LARGE_NUMBER, configFile);
    for(i = 0; in[i] != '\n'; i++);
    in[i] = '\0';

    numDivisions = atoi(in);
  printf("%i\n", numDivisions);
  */
  
  //Get zero indexed location of 'X'
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';

  x = atoi(in);
printf("%i\n", x);

  //Get zero indexed location of 'Y'
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';

  y = atoi(in);
printf("%i\n", y);

  //Get zero indexed location of 'Z'
  fgets(in, LARGE_NUMBER, configFile);
  for(i = 0; in[i] != '\n'; i++);
  in[i] = '\0';

  z = atoi(in);
printf("%i\n", z);
  
  //Mother-effin done with the config file
  fclose(configFile);
  printf("Finished load from file\n");

  //If we didn't get a template file, this ain't gonna work
  if (templateFile == NULL) {
    puts("Template File does not exist\n");
    exit(1);
  }

  fgets(in, LARGE_NUMBER, templateFile);
  for(i = 0, template_elements = 0; in[i] != '\n' && in[i] != NUL; ) {
    for ( ; (in[i] == ' ' || in[i] == TAB || in[i] == ',') && in[i] != '\n' && in[i] != NUL; i++) {
      // This advances through the delimiter area.  This works if the delimiter is mulitple characters, like "  " or ", ".
      if (delim == '\0') delim = in[i]; // Save the delimiter char for later use if it's after the first element.
    }
    for ( ; in[i] != ' ' && in[i] != TAB && in[i] != ',' && in[i] != '\n' && in[i] != NUL; i++); 
      // This advances through the element name.
    template_elements++;  // Once we reach the end of an element name, increment the rows counter.
  }


  printf("Delimiting Char: %d (%c)\n", delim, delim);
  attributes = (int*)malloc(template_elements * sizeof(int));

  /*
  * This looks kinda magical if you don't know what's going on (trust me, I had to re-figure it out). Later we have an array that we call that has indexes of which column
  * holds x, y, z, and each attribute. 'k' holds this integer index, 'i' holds the actual text of what the template calls the field so we can compare it to some known values below
  */
  printf("%s\n", in);
  for(i = 0, k = 0, numAttributes = 0; in[i] != '\n' && in[i] != NUL; i++, k++) { //k is our resident index counter for which "word" in the template we're on
    for(j = 0; in[i] != ' ' && in[i] != TAB && in[i] != ',' && in[i] != '\n' && in[i] != NUL; i++, j++)
      tempString[j] = in[i] | 0x20; //This is a nifty little trick ensuring all characters are lower case so we don't have to test for multiple cases (bitwise OR with hex "20")
    tempString[j] = '\0';
    printf("%s = ", tempString);
    if(!strcmp(tempString, "x") || !strcmp(tempString, "longitude")){
      x = k;
      puts("x");
    } else if(!strcmp(tempString, "y") || !strcmp(tempString,"latitude")){
        y = k;
        puts("y");
    } else if(!strcmp(tempString, "z") || !strcmp(tempString, "depth")){
        z = k;
        puts("z");
    } else if(!strcmp(tempString, "date")) {
        date = k;
        puts("date");
    } else if(!strcmp(tempString, "time")){
        time = k;
        puts("time");
    } else if (strcmp(tempString, "n/a") && strcmp(tempString, "done")) { //This is wonky since strcmp returns 0 for true this means if its not "n/a" or "done" then add it to list of attributes
        attributes[numAttributes] = k;
        numAttributes++;
        puts("d");
    }

    /*if (strcmp(tempString, "n/a") && strcmp(tempString, "done")) { //This is wonky since strcmp returns 0 for true this means if its not "n/a" or "done" then add it to list of attributes
      attributes[numAttributes] = k;
      numAttributes++;
      puts("d");
    }*/
  }
 

  //Make sure we have a valid X, Y, and Z
  if (x == -1 || y == -1 || z == -1){
    puts("One or more of the XYZ values not set, check your template file and try again");
    exit(1);
  }

  printf("file dimensions\n");////////////////////////
  //Get length of file. I hate doing it this way, running through the file twice is so wasteful
  if (infile == NULL) {
    puts("Infile -- File does not exist\n");
    exit(1);
  }

  //Get file dimensions
  fgets(tempString, LARGE_NUMBER, infile);
  for (i = 0; tempString[i] != '\0'; i++)
    if (tempString[i] == delim && tempString[i+1] != delim && tempString[i+1] != '\n' && tempString[i+1] != '\0') 
      cols++;
  for (rows = 1; fgets(in, LARGE_NUMBER, infile) != NULL; rows++);
  rewind(infile);

  if (cols != template_elements){
    printf("Cols: %i Template Elements: %i\n", cols, template_elements);
    puts("Elements in template file not equal to elements in data file, exiting now");
    exit(1);
  }


  puts("data\n");

  printf("Rows: %li Cols: %i\n", rows, cols);
  if ((data = (float*)malloc(cols * sizeof(float))) == NULL)
    printf ("Malloc failed!\n");


  rewind(infile); 

  divRows = rows/numDivisions; //This is the crux of a static number time division

  /*=====Meat of program=====*/
  //BEGIN!
  for (div = 0; div < numDivisions; div++) {
    printf("begin\n");//////////////////////////////
   
    //Create filename
    if (div == 0 && numDivisions > 1){
      char doThis[256];
      sprintf(doThis, "mkdir %s", title);
      system(doThis);
    } 
    if (numDivisions > 1)
      sprintf(outputFileName, "%s/%s_%i%s\0", title, title, div, EXTENSION); //We're creating a bunch of files and we've already given them a directory
    else {
      sprintf(outputFileName, "%s%s\0", title, EXTENSION); 
      sprintf(attributeFileName, "%sattr%s", title, EXTENSION);
    }

    //Open file
    outfile = fopen(outputFileName, "w");
    attributesFile = fopen(attributeFileName, "w");

    //Write header
    fprintf(outfile, LINE1);
    fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
    fprintf(outfile, LINE3);
    fprintf(outfile, LINE4);
    fprintf(outfile, LINE5, divRows);

    //Write "header" for attribute files
    fprintf(attributesFile, "\n%s %li\n", "POINT_DATA", realRows);
    for (i = 0; i < numAttributes; i++) {
        rewind(templateFile);
        fgets(in, LARGE_NUMBER, templateFile);
        tok = strtok(in, VALID_DELIMETERS);
        k = -1;
        do {
          k++;
          strcpy(tempString, tok);
          tok = strtok(NULL, VALID_DELIMETERS);
        } while (k < attributes[i] && tok != NULL);

        fprintf(attributesFile, "%s %s %s %i\n", "SCALARS", tempString, "float", 1);
        fprintf(attributesFile, "%s %s\n", "LOOKUP_TABLE", "default");
        fseek(infile, readStart, SEEK_SET);
    }
    //Get one line of data and put into in[]
    readStart = ftell(infile);
    for (point = 0, realRows = 0; fgets(in, LARGE_NUMBER, infile) != NULL && point < divRows; point++) {
      for(k = 0, tok = strtok(in, VALID_DELIMETERS); k < cols && tok != NULL; k++, tok = strtok(NULL, VALID_DELIMETERS)) {
        if(tok[strlen(tok)] == '\n') {tok[strlen(tok)] = '\0';}
        data[k] = (isnum(tok)) ? atof(tok) : 0;
      }

      //Write Data
      //Write XYZ to outfile
      fprintf(outfile, "%f ", data[x]);
      fprintf(outfile, "%f ", data[y]);
      fprintf(outfile, "%f\n", data[z]);

      //Write Attributes to attributes file
      ///////////!!!!!+++++HARDCODED SOMETHING. BAD BAD NICK!
      fprintf(attributesFile, "%f\n", data[attributes[0]]); //attributes holds an int array with indices to which elements of data are attributes

      realRows++; //Keeping track of how many datapoints we've really written so we can correct the header if its wrong
    }
    
    //Write Attributes
    if (numAttributes) {
      fprintf(outfile, "%s %li %li\n", "CELLS", realRows, 2*realRows);
      for (i = 0; i < realRows; i++)
        fprintf(outfile, "%i %i\n", 1, i);
      
      fprintf(outfile, "%s %li\n", "CELL_TYPES", realRows);
      for(i = 0; i < realRows; i++)
        fprintf(outfile, "%i\n", 1);
      
      /*
      fprintf(outfile, "%s %s %li\n", "LOOKUP_TABLE", "default", rows);
      for (i = 0; i < realRows; i++) {
        fprintf(outfile, "%f ", (data[i][red])/10000);
        fprintf(outfile, "%f ", (data[i][green])/10000);
        fprintf(outfile, "%f ", (data[i][blue])/10000);
        fprintf(outfile, "%f", 1.0);
        fprintf(outfile, "%c", '\n');
      } 
      */
      /*fprintf(outfile, "\n%s %li\n", "POINT_DATA", realRows);
      for (i = 0; i < numAttributes; i++) {
        rewind(templateFile);
        fgets(in, LARGE_NUMBER, templateFile);
        tok = strtok(in, VALID_DELIMETERS);
        k = -1;
        do {
          k++;
          strcpy(tempString, tok);
          tok = strtok(NULL, VALID_DELIMETERS);
        } while (k < attributes[i] && tok != NULL);

        fprintf(outfile, "%s %s %s %i\n", "SCALARS", tempString, "float", 1);
        fprintf(outfile, "%s %s\n", "LOOKUP_TABLE", "default");
        fseek(infile, readStart, SEEK_SET);
        for(j = 0; fgets(in, LARGE_NUMBER, infile) != NULL && j < realRows; j++) {
          for(k = 0, tok = strtok(in, VALID_DELIMETERS); tok != NULL && k < cols; k++, tok = strtok(NULL, VALID_DELIMETERS)) {
            //if(tok[strlen(tok)-1] == '\n') tok[strlen(tok)-1] = '\0';
            data[k] = (isnum(tok)) ? atof(tok) : 0;
          }

          fprintf(outfile, "%f\n", data[attributes[i]]); //attributes holds an int array with indices to which elements of data are attributes
        }
        fprintf(outfile, "\n");
      } */
    }
    
    if (realRows != divRows) { //If we didn't really write as many points in the file as the header says, rewrite the header with the right number
      rewind(outfile);
      fprintf(outfile, LINE1);
      fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
      fprintf(outfile, LINE3);
      fprintf(outfile, LINE4);
      fprintf(outfile, LINE5, realRows);

    /*
    if (realRows != divRows) { //If we didn't really write as many points in the file as the header says, rewrite the header with the right number
      int temp = divRows;
      for(i = 0; temp != 0; i++)
        temp/10;

      rewind(outfile);
      fprintf(outfile, LINE1);
      fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
      fprintf(outfile, LINE3);
      fprintf(outfile, LINE4);
      fprintf(outfile, "POINTS %i.0li float\n", realRows);
      */
    }

    fclose(attributesFile);
    fclose(outfile);

    char doThis[256];
    sprintf(doThis, "cat %s %s > %scat%s", outputFileName, attributeFileName, outputFileName, EXTENSION);
    system(doThis);
  }
  puts("\ndone\n");
  fclose(infile);
  fclose(templateFile);
  free(attributes);
  free(data);

  return 0;
}
