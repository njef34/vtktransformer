////////////////////////////////
// This program is the intellectual property of
//     NICHOLAS J EGGLESTON
// Created for the Missouri University of Science and Technology
//     IT Research Support Services
//     and Mark Bookout, Director of RSS
// 
// Project: V4DiR (Visualizing 4 Dimensions in Rolla)
// Filename: memoryvtk.c

//////////////TESTING ONLY
//////////////PURPOSE: TO SEE IF ITS VIABLE TO READ THE WHOLE FILE INTO MEMORY

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*======GLOBAL CONSTANTS======*/
#define INPUT_FILE "/home/nick/Desktop/Backup/DataSets/Tornado/tornado.dat"
#define TEMPLATE "/home/nick/Desktop/Backup/DataSets/Tornado/firstline_small.txt"
#define EXTENSION ".vtk"

//#define NULL '\0'  
#define TAB '\t'
#define NUL '\0'

/*======VTK HEADER========*/
#define LINE1 "# vtk DataFile Version 2.0\n"  //Datafile version
#define LINE2 "%s_%s%i\n"                     //Title-of-data_framexxx
#define LINE3 "ASCII\n"                       //Is data ASCII or Binary (In this case ASCII)
#define LINE4 "DATASET UNSTRUCTURED_GRID\n"   //We have an "Unstructured Grid" Dataset
#define LINE5 "POINTS %li float\n"            //Here's our points, here's how many we have, they're of type float


 int main() {
  printf("hi\n");//////////////////////////
  /* ===== Start Declare Vars =====*/
    printf("declarations\n");///////////////////////
    /* === Hold XYZ Data === */
    float **data = NULL;
    int x = -1;
    int y = -1;
    int z = -1;
    /* === Hold Template Data ===*/
    int template_elements;
    int *attributes;
    char delim = NUL;
    char element[500];
    /* === Hold File Data == */
    FILE *template = fopen(TEMPLATE, "r");
    FILE *infile = fopen(INPUT_FILE, "r");
    FILE *outfile;
    char title[] = "earthquake_data_one_div"; //This is sort of a const but eventually we'll prompt for user input
    char outputFileName[256];
    /* === Hold Attribute Data === */
    int red = 6;
    int green = 7;
    int blue = 8;
    int date = -1;
    int time = -1;
    int cols = 1;
    long rows = 0;
    int num_attributes = 0;
    long realRows = 0;
    long divRows;   //This holds how many rows will go in each division for a for-loop later
    /* === Other Random Containers === */
    int divisions = 1; //This holds the user specified number of divisions for their data
    int i, j, k, point;
    int div;            //This holds which division we're on currently
    char c;
    char colArray[1000];
    char in[500];
    char *tok;
  /* ===== End Declare Vars ===== */
  if (template == NULL) {
    puts("Template -- File does not exist\n");
    exit(1);
  }

  fgets(in, 500, template);
  for(i = 0, template_elements = 0; in[i] != '\n' && in[i] != NUL; ) {
    for ( ; (in[i] == ' ' || in[i] == TAB || in[i] == ',') && in[i] != '\n' && in[i] != NUL; i++) {
      // This advances through the delimiter area.  This works if the delimiter is mulitple characters, like "  " or ", ".
      if (delim == '\0') delim = in[i]; // Save the delimiter char for later use if it's after the first element.
    }
    for ( ; in[i] != ' ' && in[i] != TAB && in[i] != ',' && in[i] != '\n' && in[i] != NUL; i++); 
      // This advances through the element name.
    template_elements++;  // Once we reach the end of an element name, increment the rows counter.
  }

  printf("Delimiting Char: %d\n", delim);
  attributes = (int*)malloc(template_elements * sizeof(int));

  for(i = 0, k = 0, num_attributes = 0; in[i] != '\n' && in[i] != NUL; i++, k++) { //k is our resident index counter for which "word" in the template we're on
    for(j = 0; in[i] != ' ' && in[i] != TAB && in[i] != ',' && in[i] != '\n' && in[i] != NUL; i++, j++)
      colArray[j] = in[i];
    colArray[j] = '\0';
    printf("%s = ", colArray);
    if(!strcmp(colArray, "x") || !strcmp(colArray, "X") || !strcmp(colArray, "Longitude")){
          x = k;
          puts("x");
      } else if(!strcmp(colArray, "y") || !strcmp(colArray, "Y") || !strcmp(colArray, "Latitude")){
          y = k;
          puts("y");
      } else if(!strcmp(colArray, "z") || !strcmp(colArray, "Z") || !strcmp(colArray, "Depth")){
          z = k;
          puts("z");
      } else if(!strcmp(colArray, "date") || !strcmp(colArray, "Date")){
          date = k;
          puts("date");
      } else if(!strcmp(colArray, "time") || !strcmp(colArray, "Time")){
          time = k;
          puts("time");
      } else if (strcmp(colArray, "n/a") && strcmp(colArray, "N/A") && strcmp(colArray, "done") && strcmp(colArray, "Done")) { //This is wonky since strcmp returns 0 for true this means if its not "n/a" or "done" then add it
          attributes[num_attributes] = k;
          num_attributes++;
          puts("d");
      }
  }


  printf("file dimensions\n");////////////////////////
  //Get length of file. I hate doing it this way, running through the file twice is so wasteful
  if (infile == NULL) {
    puts("Infile -- File does not exist\n");
    exit(1);
  }
  
  //Get file dimensions
  fgets(colArray, 1000, infile);
  for (i = 0; colArray[i] != '\0'; i++)
    if (colArray[i] == delim && colArray[i+1] != delim) 
      cols++;
  for (rows = 1; fgets(in, 500, infile) != NULL; rows++);
  rewind(infile);

  if (cols != template_elements){
    printf("Cols: %i Template Elements: %i\n", cols, template_elements);
    puts("Elements in template file not equal to elements in data file, exiting now");
    exit(1);
  }
  

  puts("data\n");
  
  printf("Rows: %li Cols: %i\n", rows, cols);
  if ((data = (float**)malloc(rows * sizeof(float*))) == NULL)
    printf ("Malloc failed!1\n");
  for (i = 0; i < rows; i++) 
    if ((data[i] = (float*)malloc(template_elements * sizeof(float))) == NULL)
      printf ("Malloc failed!2\n");
  puts("done\n");

  rewind(infile); 

  divRows = rows/divisions; //This is the crux of a static number time division
  
  /*=====Meat of program=====*/
  //BEGIN!
  for (div = 0; div < divisions; div++) {
    printf("begin\n");//////////////////////////////
   
    //Create filename
    if (div == 0){
      char doThis[256];
      sprintf(doThis, "mkdir %s", title);
      system(doThis);
    } 
    if (divisions > 1)
      sprintf(outputFileName, "%s/%s_%i%s\0", title, title, div, EXTENSION); //We're creating a bunch of files and we've already given them a directory
    else
      sprintf(outputFileName, "%s%s\0", title, EXTENSION); 

    
    //Open file
    outfile = fopen(outputFileName, "w");

    //Write header
    fprintf(outfile, LINE1);
    fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
    fprintf(outfile, LINE3);
    fprintf(outfile, LINE4);
    fprintf(outfile, LINE5, divRows);
    
    //Get one line of data and put into in[] 
    
    for (k = 0, realRows = 0; 
         fgets(in, 500, infile) != NULL && k < divRows; 
         k++) {
      point = 0;
      tok = strtok(in, " \n");
      while (tok != NULL){ 
        if (tok[sizeof(tok)-1] == '\n')
          tok[sizeof(tok)-1] = '\0';
        
        data[k][point] = atof(tok);

        tok = strtok(NULL, " ,\t\n");
        point++;
      }
      realRows++; //Keeping track of how many datapoints we've really written so we can correct the header if its wrong
    }
    for (i = 0; i < realRows; i++) {
      fprintf(outfile, "%f ", data[i][x]);
      fprintf(outfile, "%f ", data[i][y]);
      fprintf(outfile, "%f", data[i][z]);
      fprintf(outfile, "%c", '\n');
    } 

    if (num_attributes) {
      fprintf(outfile, "%s %li %li\n", "CELLS", realRows, 2*realRows);
      for (i = 0; i < realRows; i++)
        fprintf(outfile, "%i %i\n", 1, i);
      
      fprintf(outfile, "%s %li\n", "CELL_TYPES", realRows);
      for(i = 0; i < realRows; i++)
        fprintf(outfile, "%i\n", 1);
      
      /*
      fprintf(outfile, "%s %s %li\n", "LOOKUP_TABLE", "default", rows);
      for (i = 0; i < realRows; i++) {
        fprintf(outfile, "%f ", (data[i][red])/10000);
        fprintf(outfile, "%f ", (data[i][green])/10000);
        fprintf(outfile, "%f ", (data[i][blue])/10000);
        fprintf(outfile, "%f", 1.0);
        fprintf(outfile, "%c", '\n');
      } 
      */
      fprintf(outfile, "\n%s %li\n", "POINT_DATA", realRows);
      rewind(template);
      for (i = 0, k = -1; i < num_attributes; i++) {
        fgets(in, 500, template);
        tok = strtok(in, " ,\t\n");
        do {
          k++;
          strcpy(colArray, tok);
          tok = strtok(NULL, " ,\t\n");
        } while (k < attributes[i] && tok != NULL);
        
        fprintf(outfile, "%s %s %s %i\n", "SCALARS", colArray, "float", 1);
        fprintf(outfile, "%s %s\n", "LOOKUP_TABLE", "default");
        for (j = 0; j < realRows; j++) {
          fprintf(outfile, "%f\n", data[j][attributes[i]]); //attributes holds an int array with indices to which elements of data are attributes
        }
        fprintf(outfile, "\n");
      } 
    }
    
    if (realRows != divRows) { //If we didn't really write as many points in the file as the header says, rewrite the header with the right number
      rewind(outfile);
      fprintf(outfile, LINE1);
      fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
      fprintf(outfile, LINE3);
      fprintf(outfile, LINE4);
      fprintf(outfile, LINE5, realRows);
    }

    fclose(outfile);
  }

  fclose(infile);
  fclose(template);
  free(attributes);
  free(tok);
  for (i = 0; i < template_elements; i++)
    free(data[i]);
  free(data);
  
  return 0;
}
