////////////////////////////////
// This program is the intellectual property of
//     NICHOLAS J EGGLESTON
// Created for the Missouri University of Science and Technology
//     IT Research Support Services
//     and Mark Bookout, Director of RSS
// 
// Project: V4DiR (Visualizing 4 Dimensions in Rolla)
// Filename: vtktransformer.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*======GLOBAL CONSTANTS======*/
#define INPUT_FILE "/home/nick/DataSets/Gao/Raw/evloc1.dat"
#define EXTENSION ".vtk"

/*======VTK HEADER========*/
#define LINE1 "# vtk DataFile Version 2.0\n"  //Datafile version
#define LINE2 "%s_%s%i\n"                    //Title-of-data_framexxx
#define LINE3 "ASCII\n"                      //Is data ASCII or Binary (In this case ASCII)
#define LINE4 "DATASET POLYDATA\n"           //We have a "Polydata" Dataset
#define LINE5 "POINTS %li float\n"           //Here's our points, here's how many we have, they're of type float


 
 /* reverse:  reverse string s in place */
 // Taken from Kernighan and Ritchie's "The C Programming Language"
 void reverse(char s[])
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

 /* itoa:  convert n to characters in s */
 // Taken from Kernighan and Ritchie's "The C Programming Language"
 void itoa(int n, char s[])
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* record sign */
         n = -n;          /* make n positive */
     i = 0;
     do {       /* generate digits in reverse order */
         s[i++] = n % 10 + '0';   /* get next digit */
     } while ((n /= 10) > 0);     /* delete it */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }




 int main() {
  printf("hi\n");//////////////////////////
  //Declare Vars
  printf("declarations\n");///////////////////////
  int i, j, k, point;
  int cols = 1;
  int ignoreColsBeg = 2; 
  int ignoreColsEnd = 1; 
  int divisions = 30; //This holds the user specified number of divisions for their data
  int div;           //This holds which division we're on currently
  long rows = 0;
  long realRows = 0;
  long divRows = 0;  //This holds how many rows will go in each division for a for-loop later
  char r;
  char colArray[1000];
  char in[500];
  char *tok, *lineptr;
  char title[] = "thisisareallylongtitletothrowthingsoff"; //This is sort of a const but eventually we'll prompt for user input
  char output_file[256];

  //Setup file I/O
  printf("i/o\n"); //////////////////////////////
  FILE *infile = fopen(INPUT_FILE, "r");
  FILE *outfile;

  printf("file dimensions\n");////////////////////////
  //Get length of file. I hate doing it this way, running through the file twice is so wasteful
  r = getc(infile);
  while (r != EOF) {
    if (r == '\n')
      rows++;  
    r = getc(infile); //Puts the file pointer back at the beginning of the file
  }
  rewind(infile);
  
  //Get num of columns
  for (i = 0; i < 1000; i++)
    colArray[i] = '\0'; //initialize array
  fgets(colArray, 1000, infile);
  for (i = 0; colArray[i] != '\0'; i++){
    if (colArray[i] == ' ' && colArray[i+1] != ' '){ 
      cols++;
    }
  }
  //////Important C99 Magic////////
  char line[1000][cols]; //This has to be declared here so the array is the right size
  
  rewind(infile); //Puts the file pointer back at the beginning of the file
  printf("%i\n", cols); /////////////////////

  divRows = rows/divisions; //This is basically the crux of how we do time divisions
  
  /*=====Meat of program=====*/
  //BEGIN!
  for (div = 0; div < divisions; div++) {
    printf("begin\n");//////////////////////////////
    //Clear out all arrays
    for (i = 0; i < 500; i++)
      in[i] = '\0';
    for (i = 0; i < 1000; i++)
      for (j = 0; j < cols; j++)
        line[i][j] = '\0';
    for(i = 0; i < 256; i++) //sizeof(output_file)-1
      output_file[i] = '\0';

    //Create filename
    if (div == 0 && divisions > 1){
      char doThis[256];
      sprintf(doThis, "mkdir %s", title);
      system(doThis);
      sprintf(output_file, "%s/%s_%i%s", title, title, div, ".vtk"); //We're creating a bunch of files, give them their own directory
    }
    else if (divisions > 1)
      sprintf(output_file, "%s/%s_%i%s", title, title, div, ".vtk"); //We're creating a bunch of files and we've already given them a directory
    else  
      sprintf(output_file, "%s_%i%s", title, div, ".vtk"); 
    
    //Open file
    outfile = fopen(output_file, "w");
    
    //Write header
    fprintf(outfile, LINE1);
    fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
    fprintf(outfile, LINE3);
    fprintf(outfile, LINE4);
    fprintf(outfile, LINE5, divRows);
    
    //Get one line of data and put into in[] 
    
    for (k = 0, realRows = 0; 
         fgets(in, 500, infile) != NULL && k < divRows; 
         k++) {
      point = 0;
      tok = strtok(in, " \n");
      while (tok != NULL){ 
        if (tok[sizeof(tok)-1] == '\n')
          tok[sizeof(tok)-1] = '\0';
          for (j = 0; tok[j] != '\0'; j++)
            line[j][point] = tok[j];
        tok = strtok(NULL, " \n");
        point++;
      }
      for (i = ignoreColsBeg; i < (cols-ignoreColsEnd); i++){
        for(j = 0; line[j][i] != '\0'; j++)
          fprintf(outfile, "%c", line[j][i]);
        fprintf(outfile, " ");
      }
      fprintf(outfile, "\n");
      realRows++;
    } 
    if (realRows != divRows) { //If we didn't really write as many points in the file as the header says, rewrite the header with the right number
      rewind(outfile);
      fprintf(outfile, LINE1);
      fprintf(outfile, LINE2, title, "frame", div); //Title-of-data_framexxx
      fprintf(outfile, LINE3);
      fprintf(outfile, LINE4);
      fprintf(outfile, LINE5, realRows);
    }
  
    fclose(outfile);
  }

  fclose(infile);
  
  return 0;
}
